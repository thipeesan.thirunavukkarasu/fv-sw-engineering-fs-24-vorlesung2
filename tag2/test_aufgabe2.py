import pytest
from aufgabe2 import reverse_string


@pytest.mark.parametrize("input,output", [("tipi", "ipit")])
def test_reverse_code(input, output):
    assert reverse_string(input) == output
