from fastapi import FastAPI
import pandas as pd
import json

app = FastAPI()

df = pd.read_csv("data/small_sample.csv")

average_kredit_amount = df["loan_amnt"].mean()


@app.get("/")
def read_home():
    return {"Hellor": "World"}


@app.get("/data")
def get_all_data():
    return json.loads(df.to_json(orient="records"))


@app.get("/kpi/average_loan_amount")
def get_average_loan_amount():
    return {"average_loan_amount": round(average_kredit_amount, 2)}


bankaccounts = [
    {"user": "Tipi", "konto": "CH123456"},
    {"user": "Felix", "konto": "CH123457"},
    {"user": "David", "konto": "CH12344532"},
]


@app.get("/bankaccounts")
def get_all_bankaccounts():
    return bankaccounts


@app.get("/bankaccounts/{index_list}")
def get_one_bank_account(index_list: int):
    return bankaccounts[index_list]
