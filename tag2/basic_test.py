import pytest
from unittest.mock import MagicMock
from addieren import hallo, add_number
from dividieren import dividieren


def test_hallo():
    assert hallo() == "hallo"


@pytest.mark.parametrize("zahl_a, zahl_b,erg", [(2, 2, 4), (2, 3, 5), (3, 3, 6)])
def test_addieren(zahl_a, zahl_b, erg):
    assert add_number(zahl_a, zahl_b) == erg


def test_dividiren():
    assert dividieren(10, 10) == 1


def test_error_dividieren():
    with pytest.raises(ZeroDivisionError):
        dividieren(10, 0)


def test_mocking_addieren():
    api = MagicMock(return_value=(42, 3, 45))
    zahla, zahlb, erg = api()
    assert add_number(zahla, zahlb) == erg
