import pytest
from unittest.mock import MagicMock
from email_service import EmailService


@pytest.fixture
def email_service_mock():
    return MagicMock(spec=EmailService)


def test_send_email(email_service_mock):
    email = "test@example.com"
    subject = "Test Email"
    body = "Dies ist ein Test E-mail"
    email_service_mock.send_email(email, subject, body)
