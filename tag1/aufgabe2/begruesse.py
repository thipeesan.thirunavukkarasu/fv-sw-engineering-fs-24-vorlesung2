from datetime import datetime


def begruesse_person(name: str, alter: int) -> None:
    print(f"Hallo {name} schön, dass du da bist!")
    if alter < 18:
        print("Du bist noch minderjährig.")
    else:
        print("Du bist volljährig.")


def berechne_jahreszahl(alter: int) -> int:
    jahr = datetime.now().year
    geburtsjahr = jahr - alter
    return geburtsjahr
