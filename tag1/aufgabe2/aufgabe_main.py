from begruesse import begruesse_person, berechne_jahreszahl

name = input("Wie ist dein Name? ")
alter = int(input("Wie alt bist du?"))

begruesse_person(name, alter)

geburtsjahr = berechne_jahreszahl(alter)


print(
    f"Wenn du in diesem Jahr schon geburtstag hattest, dann wurdest du im Jahr {geburtsjahr} geboren."
)
print(f"Ansonsten wurdest du im Jahr {geburtsjahr-1} geboren.")
