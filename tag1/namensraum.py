# globalen variable
x = 19


def a():
    # lokalen variable
    # x = 10
    print(x)


# funktions aufruf
a()


# PRIO: lokale variable > globale variable


# Regel 1: Globale variablen am Anfang nach dem import definieren
# Regel 2: Nur Konstanten als Globale Variablen definieren
# Regel 3: Globale Variablen werden in Uppercase geschrieben
