class Auto:

    # Konstruktor -> initialisierung des objects
    def __init__(self, marke: str, motor: str) -> None:
        # public variablen
        self.marke = marke
        self.motor = motor
        self.reader_anzahl = 4
        # private variablen
        self.__geschwindigkeit = 0

    # eine klassen methode
    def fahre_los(self):
        print(
            f" {self.marke} fahre los mit der geschwindigkeit {self.__geschwindigkeit}"
        )

    def gasgeben(self):
        # self.geschwindigkeit = self.geschwindigkeit + 10
        self.__geschwindigkeit += 10
        print(f" {self.marke} fährt mit der geschwindigkeit {self.__geschwindigkeit}")

    def bremsen(self):
        # self.geschwindigkeit = self.geschwindigkeit - 10
        self.__geschwindigkeit -= 10
        print(f" {self.marke} fährt mit der geschwindigkeit {self.__geschwindigkeit}")

    def vollbremsung(self):
        self.__geschwindigkeit = 0
        print(f" {self.marke} fährt mit der geschwindigkeit {self.__geschwindigkeit}")

    def halten(self):
        self.__geschwindigkeit = 0
        print(f" {self.marke} hat angehlaten")


# objekt instanzieierung von Auto
auto1 = Auto(marke="VW", motor="benziner")

auto1.fahre_los()
auto1.gasgeben()
auto1.__geschwindigkeit = 200
auto1.gasgeben()
auto1.gasgeben()
auto1.gasgeben()
auto1.bremsen()
auto1.gasgeben()
auto1.gasgeben()
auto1.vollbremsung()

# Vererbung von Klassen


class Eauto(Auto):
    def __init__(self, marke: str) -> None:
        super().__init__(marke=marke, motor="emootor")


eauto1 = Eauto("Tesla")
eauto1.fahre_los()
