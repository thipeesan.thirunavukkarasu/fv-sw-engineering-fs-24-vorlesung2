def addieren(zahl_eins: int, zahl_zwei: int) -> int:
    return zahl_eins + zahl_zwei


def subtrahieren(zahl_eins: int, zahl_zwei: int) -> int:
    return zahl_zwei - zahl_eins


print(f"Ergebnis lautet:  {addieren(2,3)}")
print(f"Ergebnis lautet:  {addieren(4,3)}")
print(f"Ergebnis lautet:  {subtrahieren(2,3)}")
