# unpacking
tupel_zahl: tuple = (2, 1, 3, 4)

print(tupel_zahl)

a = tupel_zahl[0]
b = tupel_zahl[1]
c = tupel_zahl[2]
d = tupel_zahl[3]
print(a, b, c, d)

# best practis
d, f, g, _ = tupel_zahl
print(
    d,
    f,
    g,
)

# swap
vorname, name = "Tipi", "Thiru"
print(vorname, name)
# best practis
vorname, name = name, vorname
print(vorname, name)

# loop in list
list_werte: list = []
# 0 - 100 ablegen
for v in range(100):
    list_werte.append(v)
print(list_werte)

# best pratis
list_wert2: list = [v for v in range(100)]

alter = 3
if alter < 18:
    x = "ist ein kind"
else:
    x = " ist erwachsen"

y = "ist ein kind" if alter < 18 else " ist erwachsen"
print(x)
print(y)


# zip -> Methode
spalten = ["A", "B", "C"]
werte = [1, 31, 2]

list_tabelle = list(zip(spalten, werte))
print(list_tabelle)

spalten1, werte1 = zip(*list_tabelle)

print(werte1)
