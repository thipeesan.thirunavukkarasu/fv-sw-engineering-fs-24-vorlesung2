from aufgabe_schueler import Schueler

name = input("Wie ist dein Name? ")
alter = int(input("Wie alt bist du? "))
schueler = Schueler(name, alter)
schueler.begruesse()
geburtsjahr = schueler.berechne_geburtsjahr()
print(f"Du wurdest im Jahr {str(geburtsjahr)} geboren.")
