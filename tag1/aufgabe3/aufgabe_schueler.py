class Schueler:
    def __init__(self, name, alter):
        self.name = name
        self.alter = alter

    def begruesse(self):
        print(f"Hallo {self.name} schön, dass du da bist!")
        if self.alter < 18:
            print("Du bist noch minderjährig.")
        else:
            print("Du bist volljährig.")

    def berechne_geburtsjahr(self):
        jahr = 2023
        geburtsjahr = jahr - self.alter
        return geburtsjahr
